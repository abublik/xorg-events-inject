pkgs = xcb xcb-keysyms xcb-xtest
CFLAGS = `pkg-config --cflags $(pkgs)` -g -Wall -Wextra -Wpointer-arith -Wstrict-prototypes
LIBS = `pkg-config --libs $(pkgs)`

mouse:	mouse_move.c
	$(CC) $(CFLAGS) mouse_move.c $(LIBS) -o mmove

click:	click.c
	$(CC) $(CFLAGS) click.c $(LIBS) -o click

keyb:	keyb.c
	$(CC) $(CFLAGS) keyb.c $(LIBS) -o keyb

resize:	resize.c
	$(CC) $(CFLAGS) resize.c $(LIBS) -o resize 

example:	example.c
	$(CC) $(CFLAGS) example.c $(LIBS) -o example

