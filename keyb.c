#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <xcb/xcb.h>
#include <xcb/xtest.h>
#include <xcb/xcb_keysyms.h>


xcb_keycode_t* thing_to_keycode(xcb_connection_t *c, char *thing) {
	xcb_key_symbols_t *syms = xcb_key_symbols_alloc(c);
	xcb_keycode_t *kc;
	xcb_keysym_t ks;

	ks = (int)thing[0];

	kc = xcb_key_symbols_get_keycode(syms, ks);
	return kc;
}

int main(int argc, char *argv[])
{
	int i, screenNum;
	// open the connection to the X server
	xcb_connection_t *conn = xcb_connect(NULL, &screenNum);
	if (conn == NULL) {
		fprintf(stderr, "Unable to open display\n");
		exit(1);
	}

	// xtest init
	xcb_test_get_version_cookie_t cookie = xcb_test_get_version(conn, 2, 1);
	xcb_generic_error_t *err = NULL;
	xcb_test_get_version_reply_t *xtest_reply = xcb_test_get_version_reply(conn, cookie, &err);
	if (xtest_reply) {
		fprintf(stderr, "XTest version %u.%u\n", (unsigned int)xtest_reply->major_version, (unsigned int)xtest_reply->minor_version);
		free(xtest_reply);
	}

	if (err) {
		fprintf(stderr, "XTest version error: %d", (int)err->error_code);
		free(err);
	}

	// get the screen 
	const xcb_setup_t *setup = xcb_get_setup(conn);
	xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);

	// I want the screen at index screenNum
	for (i = 0; i < screenNum; ++i) {
		xcb_screen_next(&iter);
	}

	xcb_screen_t *screen = iter.data;

    printf("\n");
    printf("Informations of screen %d:\n", screen->root);
    printf("  width.........: %d\n", screen->width_in_pixels);
    printf("  height........: %d\n", screen->height_in_pixels);
    printf("  white pixel...: %d\n", screen->white_pixel);
    printf("  black pixel...: %d\n", screen->black_pixel);
    printf("\n");

	xcb_window_t none = { XCB_NONE };

	sleep(2);
	//char str[] = "hi guys";
	char *str = argv[1];
	for (unsigned int i = 0; i <= strlen(str); i++) {
		sleep(1);
		xcb_keycode_t *c = thing_to_keycode(conn, &str[i]);
		xcb_test_fake_input(conn, XCB_KEY_PRESS, *c, XCB_CURRENT_TIME, none, 0, 0, 0);
		xcb_test_fake_input(conn, XCB_KEY_RELEASE, *c, XCB_CURRENT_TIME, none, 0, 0, 0);
		
	}


	xcb_flush(conn);
	xcb_disconnect(conn);
	return 0;
}
