#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <X11/keysym.h>
#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>
#include <xcb/xtest.h>


static void fake_motion(xcb_connection_t *c, uint8_t relative, int x, int y) {
	xcb_window_t window = { XCB_NONE };
	if (!relative) {
		window = xcb_setup_roots_iterator(xcb_get_setup(c)).data->root;
	}

	xcb_test_fake_input(c, XCB_MOTION_NOTIFY, relative, 0, window, x, y, 0);
	xcb_flush(c);
}


int main(int argc, char *argv[]) {
	if (argc == 1) {
		printf("./mmove width height\n");
		exit(1);
	}

	xcb_connection_t *conn;

	//xcb_screen_t *screen;
	//xcb_window_t win = atoi(argv[1]);

	conn = xcb_connect(NULL, NULL);

	//screen = xcb_setup_roots_iterator(xcb_get_setup(conn)).data;

	int x = 0; //atoi(argv[1]);
	int y = 0; //atoi(argv[2]);

	fake_motion(conn, 0, x, y);
	for (int i = 0; i <= 20; i++) {
		//printf("x: %d - y: %d\n", x, y);
		fake_motion(conn, 0, 10, y);
		x += 10;
		y += 20;
		sleep(1);
	}

	xcb_disconnect(conn);
	return 0;
}
