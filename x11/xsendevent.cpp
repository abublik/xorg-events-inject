#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Display *MyDisplay;
Window MyWindow;
Atom MyAtom;

static void send_event(void) {
  XClientMessageEvent xevent;
  
  // Send a ClientMessage
  xevent.type = ClientMessage;
  
  // Use the Atom we got for our custom message
  xevent.message_type = MyAtom;
  
  // I don't really use these fields, but here's an example
  // of setting them
  xevent.format = 32;
  xevent.data.l[0] = 0;
  
  // Send the ClientMessage event
  XSendEvent(MyDisplay, MyWindow, 0, 0, (XEvent *)&xevent);
}

int main (int argc, char *argv[]) {
  Atom wm_delete_window;
  Atom wm_protocols;
  
  // Open screen/window
  MyDisplay = XOpenDisplay(0);
  MyWindow = XCreateSimpleWindow(MyDisplay, DefaultRootWindow(MyDisplay),
  50, 50, 500, 400, 0, BlackPixel(MyDisplay, DefaultScreen(MyDisplay)),
  WhitePixel(MyDisplay, DefaultScreen(MyDisplay)));
  
  // We want to know about the user clicking on the close window button
  wm_delete_window = XInternAtom(MyDisplay, "WM_DELETE_WINDOW", 0);
  XSetWMProtocols(MyDisplay, MyWindow, &wm_delete_window, 1);
  wm_protocols = XInternAtom(MyDisplay, "WM_PROTOCOLS", 0);

  // We want to know about ClientMessages
  XSelectInput(MyDisplay, MyWindow, StructureNotifyMask);
  
  // Display the window
  XMapWindow(MyDisplay, MyWindow);
  
  // Get an Atom to use for my custom message. Arbitrarily name the atom
  // "MyAtom"
  MyAtom = XInternAtom(MyDisplay, "MyAtom", 0);
  
  // Send my window my custom ClientMessage event
  send_event();
  
  // Do the message loop
  for (;;)  {
  XEvent event;
  
  XNextEvent(MyDisplay, &event);
  switch (event.type) {
  case ClientMessage: {
    XClientMessageEvent *evt;
    
    evt = (XClientMessageEvent *)&event;
    
    // User wants to close the window?
    if (evt->message_type == wm_protocols && evt->data.l[0] == wm_delete_window) goto out;
  
// Is it my custom message?
if (evt->message_type == MyAtom)
{
// Here I would do something about my custom message
}
}
}
}
out:

XCloseDisplay(MyDisplay);
return(0);
}
