#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <X11/keysym.h>
#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>
#include <xcb/xtest.h>


// A full list of available codes can be found in /usr/include/X11/keysymdef.h   
// /usr/include/xcb/xproto.h
void resize(xcb_connection_t *conn, xcb_window_t window, int width, int height) {
	xcb_configure_notify_event_t *event = calloc(32, 1);

	event->event = window;
	event->window= window;
	event->response_type = XCB_CONFIGURE_NOTIFY;

	//event->x = 0;
	//event->y = 0;
	event->width = width;
	event->height = height;

	event->border_width = 0;
	event->above_sibling = XCB_NONE;
	event->override_redirect = false;

	xcb_send_event(conn, false, window, XCB_EVENT_MASK_STRUCTURE_NOTIFY, (char*)event);
	xcb_flush(conn);
	free(event);
}


int main(int argc, char *argv[]) {
	if (argc == 1) {
		printf("xdotool getactivewindow\n");
		printf("./resize winnumber width height\n");
		exit(1);
	}

	xcb_connection_t *conn;

	//xcb_screen_t *screen;
	xcb_window_t win = atoi(argv[1]);

	conn = xcb_connect(NULL, NULL);

	//screen = xcb_setup_roots_iterator(xcb_get_setup(conn)).data;

	int width = atoi(argv[2]);
	int height = atoi(argv[3]);

	for (int i = 0; i <= 10; i++) {
		printf("width: %d - height: %d\n", width, height);
		resize(conn, win, width, height);
		width += 10;
		height += 10;
		sleep(1);
	}

	xcb_disconnect(conn);
	return 0;
}
